#pragma once
#include "neo.h"

typedef struct {
  uint32_t color;
  float x;
  float vel;
  bool dead;
} wave_t;

const int MAX_WAVES = 50;
const int WAVE_MARGIN = 10;
const uint32_t MASK = 0xFF;

extern wave_t waves[];

void reset_wave(int i);
void do_wave();
