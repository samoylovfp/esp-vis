#include "waves2.h"

surface_t surface[LED_COUNT];
const int PALETTE_SIZE = 4;
float step = 1.0 / PALETTE_SIZE;

uint8_t palette[PALETTE_SIZE][3] = {
    {0x00, 0x00, 0x40},
    {0x00, 0x20, 0x80},
    {0x00, 0x40, 0x160},
    {0x60, 0x80, 0xFF}
};

void calc_forces();
void apply_forces();
void display_surface();

void do_wave_surface() {
    calc_forces();
    apply_forces();
    display_surface();
}

void init_surface() {
    for(int i = 0; i<LED_COUNT; i++) {
        surface[i].height = 0.4;
        surface[i].velo = 0.0;
        surface[i].force = 0.0;
    }
}

void calc_forces() {
    for (int i = 0; i < LED_COUNT; i++) {
        surface_t *s = &surface[i];
        s->force = 0;
        s->force += (0.4 - s->height) * 0.1;
        if (i > 0) {
            s->force += (surface[i - 1].height - s->height);
        }
        if (i < LED_COUNT - 1) {
            s->force += (surface[i + 1].height - s->height);
        }
        s->force = min(max(s->force, -0.5f), 0.5f);
        s->velo += (s->force / 5.0);
        s->velo *= 0.97;
    }
    int disturbance = random(LED_COUNT - 1);
    surface[disturbance].velo += 0.25 / random(1, 10);
}

void apply_forces() {
    for (int i = 0; i < LED_COUNT; i++) {
        surface[i].height += surface[i].velo;
        //surface[i].height = (float) i / LED_COUNT;
    }
}

void display_surface() {
    for (int i = 0; i < LED_COUNT; i++) {
        float h = min(max(surface[i].height, 0.0f), 1.0f);
        uint8_t result_color[3] = {0, 0, 0};
        for (int pc_i = 0; pc_i < PALETTE_SIZE; pc_i++) {
            float influence = fmax(
                1.0 - fabs(h * (PALETTE_SIZE - 1) - pc_i),
                0.0
            );
            for (int c_c = 0; c_c < 3; c_c++) {
                result_color[c_c] += palette[pc_i][c_c] * influence;
            }
        }
        strip.setPixelColor(
            i,
            result_color[0],
            result_color[1],
            result_color[2]
        );
    }
    strip.show();
}
