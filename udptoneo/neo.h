#pragma once

#include <Adafruit_NeoPixel.h>

#define LED_PIN    0
#define LED_COUNT 77
const int STATE_SIZE = LED_COUNT * 3;

extern uint8_t ledTarget[];

// Declare our NeoPixel strip object:
extern Adafruit_NeoPixel strip;
