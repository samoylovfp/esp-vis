#include "waves.h"

wave_t waves[MAX_WAVES];

float frandom(int, int);
uint8_t getUint8(uint32_t, int);
uint8_t sadd8(uint8_t, uint8_t);


void reset_wave(int index) {
  wave_t *w = &waves[index];
  w -> x = (LED_COUNT + WAVE_MARGIN * 2) * random(0, 2) - WAVE_MARGIN;
  w -> vel = frandom(50, 100);
  
  if (w -> x > LED_COUNT) {
    w -> vel *= -1;
  }
  // intensity
  float i = frandom(10, 50);
  switch (random(0, 3)) {
    case 0:
      w->color = strip.Color(255 * i, 50 * i, 0);
    break;
    case 1:
      w->color = strip.Color(255 * i, 0, 0);
    break;
    case 2:
      w->color = strip.Color(255 * i, 100 * i, 0);
    break;
  }
}

void do_wave() {
  // reset screen buffer
  for (int i = 0; i < STATE_SIZE; i++) {
    ledTarget[i] = 0;
  }

  // update
  for(int i = 0; i < MAX_WAVES; i++) {
    wave_t *w = &waves[i];

    // draw
    for (int j = (int)w->x - WAVE_MARGIN; j < (int)w->x + WAVE_MARGIN; j++) {
      
      float w_prob = 1.0 / fmax(fabs((float) j - w->x), 1.0);
      
      if (j >= 0 && j < LED_COUNT) {
        ledTarget[j*3 + 0] = sadd8(ledTarget[j*3 + 0], w_prob * getUint8(w -> color, 16));
        ledTarget[j*3 + 1] = sadd8(ledTarget[j*3 + 1], w_prob * getUint8(w -> color, 8));
        ledTarget[j*3 + 2] = sadd8(ledTarget[j*3 + 2], w_prob * getUint8(w -> color, 0));
      }
    }

    // advance
    w->x += w->vel;

    // reset
    if ((w->x > LED_COUNT + 2*WAVE_MARGIN) || (w->x < - 2*WAVE_MARGIN)) {
      reset_wave(i);
    }
  }
  
  for (int d = 0; d < LED_COUNT; d++){
    strip.setPixelColor(d, ledTarget[d*3], ledTarget[d*3 + 1], ledTarget[d*3 + 2]);
  }
  strip.show();
}

uint8_t sadd8(uint8_t a, uint8_t b){
  return (a > 0xFF - b) ? 0xFF : a + b;
}

int fclamp(float val) {
  return round(min(max(val, (float)0.01), (float)254.9));
}

float frandom(int from, int to) {
  return random(from, to) / 1000.0;
}

uint8_t getUint8(uint32_t color, int offset) {
  return (color & (MASK << offset)) >> offset;
}
