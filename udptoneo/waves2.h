#pragma once
#include "neo.h"

typedef struct {
    float height;
    float velo;
    float force;
} surface_t;

void init_surface();
void do_wave_surface();
