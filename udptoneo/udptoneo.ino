#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <NTPClient.h>

#include "wifi_cred.h"
const char* ssid = STASSID;
const char* password = STAPSK;

WiFiUDP UdpData;
WiFiUDP UdpCommands;
WiFiUDP UdpNtp;

NTPClient timeClient(UdpNtp);

#include "waves.h"
#include "waves2.h"

void setup() {
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }
  ArduinoOTA.begin();
  UdpData.begin(8666);
  UdpCommands.begin(8668);
  timeClient.begin();
  timeClient.setTimeOffset(3600 * 3);
  timeClient.setUpdateInterval(3600000);

  for(int i = 0; i < STATE_SIZE; i++) {
    ledTarget[i] = 0;
  }
/*
  for(int i = 0; i < MAX_WAVES; i++) {
    reset_wave(i);
    waves[i].x = random(1000) / 1000.0 * LED_COUNT;
  }
*/
  strip.begin();
  strip.setBrightness(0x19);

  init_surface();
  // blink once
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  delay(100);
  digitalWrite(LED_BUILTIN, HIGH);
  
}

void loop() {
  /*
  while(!timeClient.update()) {
    timeClient.forceUpdate();
  }
*/
  
  ArduinoOTA.handle();
  
  do_wave_surface();
  // do_wave();
  delay(10);
  // showTime();
  // showFromNetwork();
}

void showTime() {
  printNumToLed(timeClient.getSeconds(), 0, 7, 0, 255, 255);
  strip.setPixelColor(7, 100, 0, 0);
  printNumToLed(timeClient.getMinutes(), 8, 7, 255, 255, 0);
  strip.setPixelColor(15, 100, 0, 0);
  printNumToLed(timeClient.getHours(), 16, 7, 255, 100, 255);

  strip.setPixelColor(30, 100, 0, 0);
  printNumToLed(timeClient.getDay(), 31, 10, 100, 0, 255);
  strip.show();
}

void printNumToLed(int num, int offset, int length, uint8_t r, uint8_t g, uint8_t b) {
  for (int i = 0; i < length; i++) {
    int is_set = ((num & (1 << i)) != 0);
    if (is_set) {
      strip.setPixelColor(offset + i, r, g, b);
    } else {
      strip.setPixelColor(offset + i, 0, 0, 0);
    }
  }
}

void showFromNetwork() {
  int p = UdpData.parsePacket();
  if (p >= STATE_SIZE) {
    for(int i = 0; i < STATE_SIZE; i++) {
      ledTarget[i] = UdpData.read();
    }
    for(int i = 0; i < strip.numPixels(); i++) {
      strip.setPixelColor(i, ledTarget[i*3], ledTarget[i*3 + 1], ledTarget[i*3 + 2]);
    }
    strip.show();
  }
}
